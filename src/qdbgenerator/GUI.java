package qdbgenerator;

public class GUI extends javax.swing.JFrame {
    Sys system = new Sys();
    MySQL_DB db = new MySQL_DB();
    javax.swing.JPopupMenu popupMenu;
    /** Creates new form GUI */
    public GUI() {
        initComponents();
        java.awt.event.MouseListener jTable_RegistryMouseListener = new java.awt.event.MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent mc) {
                if (mc.getButton() == java.awt.event.MouseEvent.BUTTON3 && jTable1.getSelectedRowCount() < 1) {
                    java.awt.Point coords = mc.getPoint();
                    int rowNumber = jTable1.rowAtPoint(coords);
                    jTable1.getSelectionModel().setSelectionInterval(rowNumber, rowNumber);
                }
            }
        };
        jTable1.addMouseListener(jTable_RegistryMouseListener);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTextArea_Excerpt.setWrapStyleWord(true);
        setTable();
        getTableDataFromDB();
        setTitle("Georgia MotorCycle Test Question Creator");
        newActionPerformed();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup_Menu = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jTextField_Question = new javax.swing.JTextField();
        jCheckBox_R1 = new javax.swing.JCheckBox();
        jTextField_Response1 = new javax.swing.JTextField();
        jCheckBox_R2 = new javax.swing.JCheckBox();
        jTextField_Response2 = new javax.swing.JTextField();
        jCheckBox_R3 = new javax.swing.JCheckBox();
        jTextField_Response3 = new javax.swing.JTextField();
        jCheckBox_R4 = new javax.swing.JCheckBox();
        jTextField_Response4 = new javax.swing.JTextField();
        jCheckBox_R5 = new javax.swing.JCheckBox();
        jTextField_Response5 = new javax.swing.JTextField();
        jButton_Add = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSpinner_ID = new javax.swing.JSpinner();
        jTextField_Reference = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jButton_New = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea_Excerpt = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem_Exit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem3 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem4 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem5 = new javax.swing.JRadioButtonMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(800, 800));

        jLabel1.setText("Question:");

        jTextField_Question.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_QuestionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_QuestionFocusLost(evt);
            }
        });

        jCheckBox_R1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_R1ActionPerformed(evt);
            }
        });
        jCheckBox_R1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jCheckBox_R1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCheckBox_R1FocusLost(evt);
            }
        });

        jTextField_Response1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Response1ActionPerformed(evt);
            }
        });
        jTextField_Response1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_Response1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_Response1FocusLost(evt);
            }
        });

        jCheckBox_R2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_R2ActionPerformed(evt);
            }
        });
        jCheckBox_R2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jCheckBox_R2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCheckBox_R2FocusLost(evt);
            }
        });

        jTextField_Response2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Response2ActionPerformed(evt);
            }
        });
        jTextField_Response2.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_Response2FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_Response2FocusLost(evt);
            }
        });

        jCheckBox_R3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_R3ActionPerformed(evt);
            }
        });
        jCheckBox_R3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jCheckBox_R3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCheckBox_R3FocusLost(evt);
            }
        });

        jTextField_Response3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Response3ActionPerformed(evt);
            }
        });
        jTextField_Response3.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_Response3FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_Response3FocusLost(evt);
            }
        });

        jCheckBox_R4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_R4ActionPerformed(evt);
            }
        });
        jCheckBox_R4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jCheckBox_R4FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCheckBox_R4FocusLost(evt);
            }
        });

        jTextField_Response4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Response4ActionPerformed(evt);
            }
        });
        jTextField_Response4.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_Response4FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_Response4FocusLost(evt);
            }
        });

        jCheckBox_R5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_R5ActionPerformed(evt);
            }
        });
        jCheckBox_R5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jCheckBox_R5FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jCheckBox_R5FocusLost(evt);
            }
        });

        jTextField_Response5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_Response5ActionPerformed(evt);
            }
        });
        jTextField_Response5.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_Response5FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_Response5FocusLost(evt);
            }
        });

        jButton_Add.setText("Save");
        jButton_Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_AddActionPerformed(evt);
            }
        });

        jScrollPane1.setAutoscrolls(true);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Question", "Response1", "Response2", "Response3", "Response4", "Response5", "Answer", "Reference", "Catagory", "Excerpt"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(jTable1);
        jTable1.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

        jLabel2.setText("Response 1");

        jLabel3.setText("Response 2");

        jLabel4.setText("Response 3");

        jLabel5.setText("Response 4");

        jLabel6.setText("Response 5");

        jSpinner_ID.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jSpinner_IDFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jSpinner_IDFocusLost(evt);
            }
        });

        jTextField_Reference.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                jTextField_ReferenceFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextField_ReferenceFocusLost(evt);
            }
        });

        jLabel7.setText("Reference");

        jLabel8.setText("Catagory");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Preparing To Ride", "Ride Within Your Abilities", "Being In Shape To Ride", "Earning Your License", "The Rider Skills Test", "Requirements", "Tips For New Riders", "Signs Signals And Markings" }));

        jButton_New.setText("New");
        jButton_New.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NewActionPerformed(evt);
            }
        });

        jLabel9.setText("Excerpt:");

        jTextArea_Excerpt.setColumns(20);
        jTextArea_Excerpt.setLineWrap(true);
        jTextArea_Excerpt.setRows(5);
        jScrollPane2.setViewportView(jTextArea_Excerpt);

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("New Question");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Save Question");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);
        jMenu1.add(jSeparator1);

        jMenuItem_Exit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem_Exit.setText("Exit");
        jMenuItem_Exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_ExitActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_Exit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenu3.setText("Set Correct Response");

        jRadioButtonMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_1, java.awt.event.InputEvent.CTRL_MASK));
        buttonGroup_Menu.add(jRadioButtonMenuItem1);
        jRadioButtonMenuItem1.setText("Response 1");
        jRadioButtonMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem1);

        jRadioButtonMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_2, java.awt.event.InputEvent.CTRL_MASK));
        buttonGroup_Menu.add(jRadioButtonMenuItem2);
        jRadioButtonMenuItem2.setText("Response 2");
        jRadioButtonMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem2);

        jRadioButtonMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_3, java.awt.event.InputEvent.CTRL_MASK));
        jRadioButtonMenuItem3.setText("Response 3");
        jRadioButtonMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem3);

        jRadioButtonMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_4, java.awt.event.InputEvent.CTRL_MASK));
        jRadioButtonMenuItem4.setText("Response 4");
        jRadioButtonMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem4);

        jRadioButtonMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_5, java.awt.event.InputEvent.CTRL_MASK));
        jRadioButtonMenuItem5.setText("Response 5");
        jRadioButtonMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem5ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem5);

        jMenu2.add(jMenu3);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton_New, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addGap(143, 143, 143)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField_Reference, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton_Add, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSpinner_ID))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel6))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jCheckBox_R5)
                                            .addComponent(jCheckBox_R4)
                                            .addComponent(jCheckBox_R3)
                                            .addComponent(jCheckBox_R2)
                                            .addComponent(jCheckBox_R1)))
                                    .addComponent(jLabel9))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField_Response5, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                            .addComponent(jTextField_Response4, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                            .addComponent(jTextField_Response3, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                            .addComponent(jTextField_Response2, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                            .addComponent(jTextField_Response1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
                            .addComponent(jScrollPane2)
                            .addComponent(jTextField_Question))))
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 804, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField_Question, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinner_ID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField_Response1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBox_R1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField_Response2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBox_R2)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTextField_Response3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCheckBox_R3)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jCheckBox_R4)
                        .addComponent(jTextField_Response4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jTextField_Response5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jCheckBox_R5))
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_Add)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jTextField_Reference, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jButton_New))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_AddActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        //Validate Input
        String id = jSpinner_ID.getValue().toString();
        String question = jTextField_Question.getText();
        String resp1 = jTextField_Response1.getText();
        String resp2 = jTextField_Response2.getText();
        String resp3 = jTextField_Response3.getText();
        String resp4 = jTextField_Response4.getText();
        String resp5 = jTextField_Response5.getText();
        String answer = null;
        String ref = jTextField_Reference.getText();
        String cat = jComboBox1.getSelectedItem().toString();
        String exr = jTextArea_Excerpt.getText();
        if(question.contains("'")) {
            question = question.replaceAll("'", "\\\\'");
        }
        if(resp1.contains("'")) {resp1 = resp1.replaceAll("'", "\\\\'");}
        if(resp2.contains("'")) {resp2 = resp2.replaceAll("'", "\\\\'");}
        if(resp3.contains("'")) {resp3 = resp3.replaceAll("'", "\\\\'");}
        if(resp4.contains("'")) {resp4 = resp4.replaceAll("'", "\\\\'");}
        if(resp5.contains("'")) {resp5 = resp5.replaceAll("'", "\\\\'");}
        if(ref.contains("'")) {ref = ref.replaceAll("'", "\\\\'");}
        if(exr.contains("'")) {exr = exr.replaceAll("'", "\\\\'");}
        if(exr.contains("\n")) {exr = exr.replaceAll("\n", "</br>");}
        if(exr.contains("•")) {exr = exr.replaceAll("•", "&bull;");}
        if(exr.contains("\"")) {exr = exr.replaceAll("\"", "&quot;");}
        
        if(jCheckBox_R1.isSelected()){
            answer = resp1;
        } else if(jCheckBox_R2.isSelected()){
            answer = resp2;
        } else if(jCheckBox_R3.isSelected()){
            answer = resp3;
        } else if(jCheckBox_R4.isSelected()){
            answer = resp4;
        } else if(jCheckBox_R5.isSelected()){
            answer = resp5;
        } else {
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            jCheckBox_R5.requestFocus();
            jCheckBox_R4.requestFocus();
            jCheckBox_R3.requestFocus();
            jCheckBox_R2.requestFocus();
            jCheckBox_R1.requestFocus();
            javax.swing.JOptionPane.showMessageDialog(this, "Please select a correct answer and resubmit.", "Save ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
            return;
        }
        // Execute DB Statement
        if(db.Connect(0)){
            String sql = "SELECT ID FROM `satalin1_GAMT`.`MCT` WHERE ID="+id;
            boolean exists;
            if(db.doesValueExist("`MCT`", "ID", id)){
                exists = true;
                sql = "UPDATE `satalin1_GAMT`.`MCT` SET Question = '"+question+"', Resp1 = '"+resp1+"', Resp2 = '"+resp2+"', Resp3 = '"+resp3+"', Resp4 = '"+resp4+"', Resp5 = '"+resp5+"', Answer = '"+answer+"', Reference = '"+ref+"', Catagory = '"+cat+"', Excerpt = '"+exr+"' WHERE ID = "+id+"";
            } else {
                exists = false;
                sql = "INSERT INTO `satalin1_GAMT`.`MCT` (`ID`, `Question`, `Resp1`, `Resp2`, `Resp3`, `Resp4`, `Resp5`, `Answer`, `Reference`, `Catagory`, `Excerpt`) VALUES ('"+id+"', '"+question+"', '"+resp1+"', '"+resp2+"', '"+resp3+"', '"+resp4+"', '"+resp5+"', '"+answer+"', '"+ref+"', '"+cat+"', '"+exr+"')";
            }
            if(db.Statement(0, sql)){
                javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)jTable1.getModel();
                id =id.replaceAll("\\\\", "");
                question = question.replaceAll("\\\\", "");
                resp1 = resp1.replaceAll("\\\\", "");
                resp2 = resp2.replaceAll("\\\\", "");
                resp3 = resp3.replaceAll("\\\\", "");
                resp4 = resp4.replaceAll("\\\\", "");
                resp5 = resp5.replaceAll("\\\\", "");
                answer = answer.replaceAll("\\\\", "");
                ref = ref.replaceAll("\\\\", "");
                exr = exr.replaceAll("</br>", "\n");
                exr = exr.replaceAll("&bull;", "•");
                exr = exr.replaceAll("&quot;", "\"");
                
                Object[] dataObject = {id,question,resp1,resp2,resp3,resp4,resp5,answer,ref,cat,exr};
                if(!exists){
                    // Add new row to table.
                    model.addRow(dataObject);
                    jTable1.updateUI();
                    // Launch New Item 
                    jButton_NewActionPerformed(evt);
                    // Setting the scrollPane to Max plus one increment.
                    int p = jTable1.getRowHeight();
                    int y = jScrollPane1.getVerticalScrollBar().getMaximum();
                    int n = (y+p);
                    jScrollPane1.getVerticalScrollBar().setMaximum(n);
                    jScrollPane1.getVerticalScrollBar().setValue(n);
                } else {
                    for(int i=0;i<dataObject.length;i++){
                        Object object = dataObject[i];
                        model.setValueAt(object, jTable1.getSelectedRow(), i);
                    }
                    jTextArea_Excerpt.setCaretPosition(0);
//                    jTable1.updateUI();
                }
            }
        }
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton_AddActionPerformed
    private void jMenuItem_ExitActionPerformed(java.awt.event.ActionEvent evt){
        System.exit(0);
    }
    private void jTextField_Response1ActionPerformed(java.awt.event.ActionEvent evt){
        
    }
    private void jTextField_Response2ActionPerformed(java.awt.event.ActionEvent evt){
        
    }
    private void jTextField_Response3ActionPerformed(java.awt.event.ActionEvent evt){
        
    }
    private void jTextField_Response4ActionPerformed(java.awt.event.ActionEvent evt){
        
    }
    private void jTextField_Response5ActionPerformed(java.awt.event.ActionEvent evt){

    }
    private void jCheckBox_R1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_R1ActionPerformed
        if(jCheckBox_R1.isSelected()){
            jCheckBox_R2.setSelected(false);
            jCheckBox_R3.setSelected(false);
            jCheckBox_R4.setSelected(false);
            jCheckBox_R5.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBox_R1ActionPerformed
    private void jCheckBox_R2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_R2ActionPerformed
        if(jCheckBox_R2.isSelected()){
            jCheckBox_R1.setSelected(false);
            jCheckBox_R3.setSelected(false);
            jCheckBox_R4.setSelected(false);
            jCheckBox_R5.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBox_R2ActionPerformed
    private void jCheckBox_R3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_R3ActionPerformed
        if(jCheckBox_R3.isSelected()){
            jCheckBox_R1.setSelected(false);
            jCheckBox_R2.setSelected(false);
            jCheckBox_R4.setSelected(false);
            jCheckBox_R5.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBox_R3ActionPerformed
    private void jCheckBox_R4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_R4ActionPerformed
        if(jCheckBox_R4.isSelected()){
            jCheckBox_R1.setSelected(false);
            jCheckBox_R2.setSelected(false);
            jCheckBox_R3.setSelected(false);
            jCheckBox_R5.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBox_R4ActionPerformed
    private void jCheckBox_R5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_R5ActionPerformed
        if(jCheckBox_R5.isSelected()){
            jCheckBox_R1.setSelected(false);
            jCheckBox_R2.setSelected(false);
            jCheckBox_R3.setSelected(false);
            jCheckBox_R4.setSelected(false);
        }
    }//GEN-LAST:event_jCheckBox_R5ActionPerformed
    private void jButton_NewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NewActionPerformed
        newActionPerformed();
    }//GEN-LAST:event_jButton_NewActionPerformed
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jButton_NewActionPerformed(evt);
    }//GEN-LAST:event_jButton1ActionPerformed
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        jButton_NewActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem1ActionPerformed
    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        jButton_AddActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem2ActionPerformed
    private void jTextField_QuestionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_QuestionFocusGained
        highliteBG(jTextField_Question);
        selectAllText(jTextField_Question);
    }//GEN-LAST:event_jTextField_QuestionFocusGained
    private void jTextField_QuestionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_QuestionFocusLost
        normalizeBG(jTextField_Question);
    }//GEN-LAST:event_jTextField_QuestionFocusLost
    private void jSpinner_IDFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jSpinner_IDFocusGained
        highliteBG(jSpinner_ID);
    }//GEN-LAST:event_jSpinner_IDFocusGained
    private void jSpinner_IDFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jSpinner_IDFocusLost
        normalizeBG(jSpinner_ID);
    }//GEN-LAST:event_jSpinner_IDFocusLost
    private void jCheckBox_R1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R1FocusGained
        highliteBG(jCheckBox_R1);
    }//GEN-LAST:event_jCheckBox_R1FocusGained
    private void jCheckBox_R1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R1FocusLost
        normalizeBG(jCheckBox_R1);
    }//GEN-LAST:event_jCheckBox_R1FocusLost
    private void jTextField_Response1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response1FocusGained
        highliteBG(jTextField_Response1);
        selectAllText(jTextField_Response1);
    }//GEN-LAST:event_jTextField_Response1FocusGained
    private void jTextField_Response1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response1FocusLost
        normalizeBG(jTextField_Response1);
    }//GEN-LAST:event_jTextField_Response1FocusLost
    private void jCheckBox_R2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R2FocusGained
        highliteBG(jCheckBox_R2);
    }//GEN-LAST:event_jCheckBox_R2FocusGained
    private void jCheckBox_R2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R2FocusLost
        normalizeBG(jCheckBox_R2);
    }//GEN-LAST:event_jCheckBox_R2FocusLost
    private void jTextField_Response2FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response2FocusGained
        highliteBG(jTextField_Response2);
        selectAllText(jTextField_Response2);
    }//GEN-LAST:event_jTextField_Response2FocusGained
    private void jTextField_Response2FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response2FocusLost
        normalizeBG(jTextField_Response2);
    }//GEN-LAST:event_jTextField_Response2FocusLost
    private void jCheckBox_R3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R3FocusGained
        highliteBG(jCheckBox_R3);
    }//GEN-LAST:event_jCheckBox_R3FocusGained
    private void jCheckBox_R3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R3FocusLost
        normalizeBG(jCheckBox_R3);
    }//GEN-LAST:event_jCheckBox_R3FocusLost
    private void jTextField_Response3FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response3FocusGained
        highliteBG(jTextField_Response3);
        selectAllText(jTextField_Response3);
    }//GEN-LAST:event_jTextField_Response3FocusGained
    private void jTextField_Response3FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response3FocusLost
        normalizeBG(jTextField_Response3);
    }//GEN-LAST:event_jTextField_Response3FocusLost
    private void jCheckBox_R4FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R4FocusGained
        highliteBG(jCheckBox_R4);
    }//GEN-LAST:event_jCheckBox_R4FocusGained
    private void jCheckBox_R4FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R4FocusLost
        normalizeBG(jCheckBox_R4);
    }//GEN-LAST:event_jCheckBox_R4FocusLost
    private void jTextField_Response4FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response4FocusGained
        highliteBG(jTextField_Response4);
        selectAllText(jTextField_Response4);
    }//GEN-LAST:event_jTextField_Response4FocusGained
    private void jTextField_Response4FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response4FocusLost
        normalizeBG(jTextField_Response4);
    }//GEN-LAST:event_jTextField_Response4FocusLost
    private void jCheckBox_R5FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R5FocusGained
        highliteBG(jCheckBox_R5);
    }//GEN-LAST:event_jCheckBox_R5FocusGained
    private void jCheckBox_R5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jCheckBox_R5FocusLost
        normalizeBG(jCheckBox_R5);
    }//GEN-LAST:event_jCheckBox_R5FocusLost
    private void jTextField_Response5FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response5FocusGained
        highliteBG(jTextField_Response5);
        selectAllText(jTextField_Response5);
    }//GEN-LAST:event_jTextField_Response5FocusGained
    private void jTextField_Response5FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_Response5FocusLost
        normalizeBG(jTextField_Response5);
    }//GEN-LAST:event_jTextField_Response5FocusLost
    private void jTextField_ReferenceFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_ReferenceFocusGained
        highliteBG(jTextField_Reference);
    }//GEN-LAST:event_jTextField_ReferenceFocusGained
    private void jTextField_ReferenceFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextField_ReferenceFocusLost
        normalizeBG(jTextField_Reference);
    }//GEN-LAST:event_jTextField_ReferenceFocusLost
    private void jRadioButtonMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem1ActionPerformed
        jCheckBox_R1.requestFocus();
        jCheckBox_R1.setSelected(true);
    }//GEN-LAST:event_jRadioButtonMenuItem1ActionPerformed
    private void jRadioButtonMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem2ActionPerformed
        jCheckBox_R2.requestFocus();
        jCheckBox_R2.setSelected(true);
    }//GEN-LAST:event_jRadioButtonMenuItem2ActionPerformed
    private void jRadioButtonMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem3ActionPerformed
        jCheckBox_R3.requestFocus();
        jCheckBox_R3.setSelected(true);
    }//GEN-LAST:event_jRadioButtonMenuItem3ActionPerformed
    private void jRadioButtonMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem4ActionPerformed
        jCheckBox_R4.requestFocus();
        jCheckBox_R4.setSelected(true);
    }//GEN-LAST:event_jRadioButtonMenuItem4ActionPerformed
    private void jRadioButtonMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem5ActionPerformed
        jCheckBox_R5.requestFocus();
        jCheckBox_R5.setSelected(true);
    }//GEN-LAST:event_jRadioButtonMenuItem5ActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
      @Override
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }
    private void jTable_MenuActionPerformed(java.awt.event.ActionEvent evt){
        int[] rows = jTable1.getSelectedRows();
        for(int r=(rows.length - 1);r >= 0;r--) {
            if(evt.getActionCommand().equals("Delete")||evt.getActionCommand().equals("Remove")) {
                String id = jTable1.getValueAt(rows[r], 0).toString();
                javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)jTable1.getModel();
                model.removeRow(jTable1.convertRowIndexToModel(rows[r]));
                jTable1.updateUI();
                if(db.Connect(0)){
                    if(db.doesValueExist("MCT", "ID", id)){
                        String sql = "DELETE FROM MCT WHERE `ID` = "+id;
                        if(db.Statement(0, sql)){
                            model.removeRow(r);
                        }
                    }
                }
            }
        }
    }
    private void setjComboBox1(String value){
        if(value.toString().equals("Preparing To Ride")) jComboBox1.setSelectedIndex(0);
        else if(value.toString().equals("Ride Within Your Abilities")) jComboBox1.setSelectedIndex(1);
        else if(value.toString().equals("Being In Shape To Ride")) jComboBox1.setSelectedIndex(2);
        else if(value.toString().equals("Earning Your License")) jComboBox1.setSelectedIndex(3);
    }
    private void newActionPerformed() {
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        jTable1.clearSelection();
        java.util.ArrayList results = db.Select(0, "Select Max(ID)from MCT");
        java.util.Map resultMap = (java.util.Map)results.get(0);
        int id = Integer.valueOf(resultMap.get("Max(ID)").toString());
        results = db.Select(0, "Select Reference, Catagory from MCT where ID = "+id);
        resultMap = (java.util.Map)results.get(0);
        String ref = resultMap.get("Reference").toString();
        String cat = resultMap.get("Catagory").toString();
        jSpinner_ID.setValue(Integer.valueOf(id+1));
        jTextField_Question.setText("");
        jTextField_Response1.setText("");
        jTextField_Response2.setText("");
        jTextField_Response3.setText("");
        jTextField_Response4.setText("");
        jTextField_Response5.setText("");
        jCheckBox_R1.setSelected(false);
        jCheckBox_R2.setSelected(false);
        jCheckBox_R3.setSelected(false);
        jCheckBox_R4.setSelected(false);
        jCheckBox_R5.setSelected(false);
        jTextField_Reference.setText(ref);
        jTextArea_Excerpt.setText("");
        setjComboBox1(cat);
        jScrollPane1.getVerticalScrollBar().setMaximum(jScrollPane1.getVerticalScrollBar().getMaximum() + jTable1.getRowHeight());
        jTextField_Question.requestFocus();
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }
    private void setTable(){
        jTable1.setColumnSelectionAllowed(false);
        jTable1.setAutoCreateRowSorter(true);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1.getTableHeader().setDefaultRenderer(new CustomHeaderRenderer());
        int cols = jTable1.getModel().getColumnCount();
        CustomTableDataRenderer custom = new CustomTableDataRenderer();
        for (int i=0;i < cols;i++) {
            jTable1.getColumnModel().getColumn(i).setCellRenderer(custom);
        }        
        popupMenu = new javax.swing.JPopupMenu();
        java.awt.Color fg = (java.awt.Color) system.getColorMap().get("menu");
        java.awt.Color bg = (java.awt.Color) system.getColorMap().get("black");
        java.awt.event.ActionListener jTable_PopupMenuActionListener = new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTable_MenuActionPerformed(evt);
            }
        };
        //MenuItems
        javax.swing.JMenuItem menuItemDelete = new javax.swing.JMenuItem();
        menuItemDelete.setText("Delete");
        menuItemDelete.setBackground(bg);
        menuItemDelete.setForeground(fg);
        menuItemDelete.addActionListener(jTable_PopupMenuActionListener);
        popupMenu.add(menuItemDelete);
        popupMenu.setBackground(bg);
        popupMenu.setForeground(fg);
        popupMenu.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        java.awt.Font font = (java.awt.Font)system.getFontMap().get("GA10");
        popupMenu.setFont(font);
        jTable1.add(popupMenu);
        jTable1.setComponentPopupMenu(popupMenu);
        enableEvents(java.awt.AWTEvent.MOUSE_EVENT_MASK);
    } 
    private void getTableDataFromDB(){
        db.Connect(0);
        String selectStatement = "Select * from MCT order by `ID`";
        java.util.ArrayList results = db.Select(0, selectStatement);
        java.util.Map dataMap;
        javax.swing.table.DefaultTableModel model = (javax.swing.table.DefaultTableModel)jTable1.getModel();
        for(int i=results.size()-1;i>=0;i--){
            dataMap = (java.util.Map) results.get(i);
            Object[] dataObject = {
                dataMap.get("ID"),
                dataMap.get("Question"),
                dataMap.get("Resp1"),
                dataMap.get("Resp2"),
                dataMap.get("Resp3"),
                dataMap.get("Resp4"),
                dataMap.get("Resp5"),
                dataMap.get("Answer"),
                dataMap.get("Reference"),
                dataMap.get("Catagory"),
                dataMap.get("Excerpt")
            };
            model.addRow(dataObject);
            jTable1.setModel(model);
        }
    }
    private void highliteBG(final java.awt.Component component){
        component.setBackground(java.awt.Color.YELLOW);
    }
    private void normalizeBG(final java.awt.Component component) {
        Thread thread = new Thread("FadeBGThread") {
            @Override
            @SuppressWarnings({"static-access", "SleepWhileInLoop"})
            public void run() {
                java.awt.Color bg;
                int bg_r,bg_g,bg_b;
                while (!component.getBackground().equals(java.awt.Color.WHITE)) {
                    try {
                        bg = component.getBackground();
                        bg_r=(bg.getRed()*3)/100 +bg.getRed()+1; if(bg_r > 255) bg_r=255;
                        bg_g=(bg.getGreen()*3)/100 +bg.getGreen()+1; if(bg_g > 255) bg_g=255;
                        bg_b=(bg.getBlue()*3)/100 +bg.getBlue()+1; if(bg_b > 255) bg_b=255;
                        component.setBackground(new java.awt.Color(bg_r, bg_g, bg_b));
                        Thread.sleep(3);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        };
        thread.start();
    }
    private void selectAllText(javax.swing.JTextField textfield){
        if(!textfield.getText().isEmpty()) {
            textfield.setSelectionStart(0);
            textfield.setSelectionEnd(textfield.getText().length());
        }
    }

    class CustomHeaderRenderer extends javax.swing.table.DefaultTableCellRenderer {
        Sys sys = new Sys();
        java.util.Map colorMap = sys.getColorMap();
        java.util.Map fontMap = sys.getFontMap();
        java.awt.Rectangle paintingRect = null,lpr = null;
        java.awt.GradientPaint gp = null, hoverGradient, columnGradient;

        public CustomHeaderRenderer() {
            super();
            setOpaque(false);
            setFont((java.awt.Font)fontMap.get("GA16B"));
            java.awt.Color menu = (java.awt.Color)colorMap.get("menu");
            java.awt.Color head = (java.awt.Color)colorMap.get("header");
            javax.swing.border.BevelBorder border = (javax.swing.border.BevelBorder) javax.swing.BorderFactory.createBevelBorder(
                    javax.swing.border.BevelBorder.RAISED,
                    menu,
                    menu.brighter(),
                    head,
                    head.brighter());
                    setBorder((border));
        }
        public void setColumnGradient(java.awt.GradientPaint gp) {
            this.columnGradient = gp;
        }
        public java.awt.GradientPaint getColumnGradient() {
            return columnGradient;
        }
        @Override
        public void paintComponent(java.awt.Graphics g) {
            java.awt.Rectangle rect = paintingRect;
            java.awt.Graphics2D g2 = (java.awt.Graphics2D)g;
                g2.setPaint(gp);
                g2.fillRect( 0, 0, rect.width, rect.height );
            super.paintComponent(g);
        }
        @Override
        public java.awt.Component getTableCellRendererComponent(javax.swing.JTable table, Object value, boolean isSelected,
            boolean hasFocus, int row, int col) {
            java.awt.Color head = (java.awt.Color)colorMap.get("header");
            int[] colAlignments = new int[]{0,0,0,0,0,0,0,0,0,0,0};
            if(colAlignments != null) setHorizontalAlignment(colAlignments[col]);
            java.awt.Rectangle rect = table.getTableHeader().getHeaderRect(col);
            gp = new java.awt.GradientPaint(0, 0, head.brighter(), 0, rect.height/2, head);
            setForeground((java.awt.Color)colorMap.get("black"));
            paintingRect = rect;
            setText( value == null ? "" : value.toString() );
            javax.swing.ImageIcon icon = new javax.swing.ImageIcon();
            setIcon(icon);
            setIconTextGap(6);
            return(this);
        }
    }
    class CustomTableDataRenderer extends javax.swing.table.DefaultTableCellRenderer {
        java.awt.Rectangle paintingRect = null;
        java.awt.GradientPaint gp = null;
        public CustomTableDataRenderer() {
            super();
            setOpaque(false);
            java.util.Map colorMap = system.getColorMap();
            java.awt.Color menu = (java.awt.Color) colorMap.get("menu");
            java.awt.Color header = (java.awt.Color) colorMap.get("header");
            javax.swing.border.BevelBorder border = (javax.swing.border.BevelBorder) javax.swing.BorderFactory.createBevelBorder(
                    javax.swing.border.BevelBorder.RAISED,
                    menu,
                    menu.brighter(),
                    header,
                    header.brighter());
            setBorder((border));
        }
        @Override
        public java.awt.Component getTableCellRendererComponent(
             javax.swing.JTable jt,
             Object value,
             boolean isSelected,
             boolean hasFocus,
             int row,
             int column
             ){
            java.util.Map colorMap = system.getColorMap();
            java.util.Map fontMap = system.getFontMap();
            java.awt.Component comp = super.getTableCellRendererComponent(jt,value,isSelected,hasFocus,row,column);
            java.awt.Color tabColor = (java.awt.Color)colorMap.get("skyblue");
            javax.swing.ImageIcon icon = new javax.swing.ImageIcon();
            setIcon(icon);
            setIconTextGap(6);
            java.awt.Rectangle rect = jt.getCellRect(row, column, isSelected);
            paintingRect = rect;
            setFont((java.awt.Font)fontMap.get("GA12B"));
            setForeground((java.awt.Color)colorMap.get("black"));
            int[] sels = jt.getSelectedRows();
            if(row % 2 != 0 && !isSelected) {
                tabColor = (java.awt.Color)colorMap.get("cloudwhite");
                if(rect != null) {
                    gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),(rect.width * 7)/8,0,tabColor.darker());
                }

            } else if(row % 2 == 0 && !isSelected) {
                tabColor = (java.awt.Color)colorMap.get("skyblue");
                if(rect != null) {
                    gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),(rect.width * 7)/8,0,tabColor.darker());
                }
            }
            if(isSelected && jTable1.hasFocus()) {
                if(value instanceof String) {
                    setFont((java.awt.Font)fontMap.get("GA14B"));
                    //Set TextField Values
                    switch(column){
                        case 0: jSpinner_ID.setValue(Integer.valueOf(value.toString()));
                        case 1: jTextField_Question.setText(value.toString());
                        case 2: jTextField_Response1.setText(value.toString());
                        case 3: jTextField_Response2.setText(value.toString());
                        case 4: jTextField_Response3.setText(value.toString());
                        case 5: jTextField_Response4.setText(value.toString());
                        case 6: jTextField_Response5.setText(value.toString());
                        case 7: {
                            jCheckBox_R1.setSelected(false);
                            jCheckBox_R2.setSelected(false);
                            jCheckBox_R3.setSelected(false);
                            jCheckBox_R4.setSelected(false);
                            jCheckBox_R5.setSelected(false);
                                if(value.toString().equals(jTextField_Response1.getText())) jCheckBox_R1.setSelected(true);
                           else if(value.toString().equals(jTextField_Response2.getText())) jCheckBox_R2.setSelected(true);
                           else if(value.toString().equals(jTextField_Response3.getText())) jCheckBox_R3.setSelected(true);
                           else if(value.toString().equals(jTextField_Response4.getText())) jCheckBox_R4.setSelected(true);
                           else if(value.toString().equals(jTextField_Response5.getText())) jCheckBox_R5.setSelected(true);
                        }
                        case 8: jTextField_Reference.setText(value.toString());
                        case 9: {
                           setjComboBox1(value.toString());
                        }
                        case 10: {
                          String exr = value.toString();
                          exr = exr.replaceAll("</br>", "\n");
                          exr = exr.replaceAll("<BR>", "\n");
                          exr = exr.replaceAll("<br>", "\n");
                          exr = exr.replaceAll("</BR>", "\n");
                          exr = exr.replaceAll("\\\\", "");
                          exr = exr.replaceAll("&bull;", "•");
                          exr = exr.replaceAll("&quot;", "\"");
                          jTextArea_Excerpt.setText(exr);
                          jTextArea_Excerpt.setCaretPosition(0);
                        }
                    }
                }
               if(rect != null) {
                    if(sels[0] == row || sels.length == 1) {
                        gp = new java.awt.GradientPaint(0,0,tabColor.darker(),0,rect.height/2,tabColor.brighter());
                    } else if(sels[jt.getSelectedRowCount() - 1] == row) {
                        gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.darker());
                    } else {
                        gp = new java.awt.GradientPaint(0,0,tabColor.brighter(),0,rect.height/2,tabColor.brighter());
                    }
                }
            }

            int[] colAlignments = new int[]{0,0,0,0,0,0,0,0,0,0,0};
            setHorizontalAlignment(colAlignments[column]);
            // ToolTip  (Middle MouseWheel Button Activates)
            java.awt.Color bg = (java.awt.Color)colorMap.get("menu");
            if(value != null){
                value = value.toString().replaceAll("</br>", "\n");
                value = value.toString().replaceAll("&bull;", "•");
                value = value.toString().replaceAll("&quot;", "\"");
                String tooltext =   "<HTML><BODY>"+
                                    "<TABLE BORDER CELLPADDING=8 BGCOLOR=\"#"+Integer.toHexString(bg.getRGB()&0x00ffffff)+"\">"+
                                    "<TH><H3>"+
                                    jt.getModel().getColumnName(column)+
                                    "</H3></TH><TD WIDTH=500><P STYLE=\"word-wrap:break-word;width:100%;left:0\"><B>"+
                                    value.toString()+
                                    "</B></P></TD></TABLE></BODY></HTML>";
                setToolTipText(tooltext);
            }
            javax.swing.ToolTipManager.sharedInstance().setInitialDelay(3750);
            javax.swing.ToolTipManager.sharedInstance().setDismissDelay(30000);
            javax.swing.ToolTipManager.sharedInstance().setReshowDelay(1200);
            return(comp);
        }
        public java.awt.GradientPaint getColumnGradient() {
            return gp;
        }
        @Override
        public void paintComponent(java.awt.Graphics g) {
            java.awt.Rectangle rect = paintingRect;
            java.awt.Graphics2D g2 = (java.awt.Graphics2D)g;
            g2.setPaint(gp);
            g2.fillRect( 0, 0, rect.width, rect.height );
            super.paintComponent(g);
        }
     }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup_Menu;
    private javax.swing.JButton jButton_Add;
    private javax.swing.JButton jButton_New;
    private javax.swing.JCheckBox jCheckBox_R1;
    private javax.swing.JCheckBox jCheckBox_R2;
    private javax.swing.JCheckBox jCheckBox_R3;
    private javax.swing.JCheckBox jCheckBox_R4;
    private javax.swing.JCheckBox jCheckBox_R5;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem_Exit;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem4;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JSpinner jSpinner_ID;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea_Excerpt;
    private javax.swing.JTextField jTextField_Question;
    private javax.swing.JTextField jTextField_Reference;
    private javax.swing.JTextField jTextField_Response1;
    private javax.swing.JTextField jTextField_Response2;
    private javax.swing.JTextField jTextField_Response3;
    private javax.swing.JTextField jTextField_Response4;
    private javax.swing.JTextField jTextField_Response5;
    // End of variables declaration//GEN-END:variables

}
